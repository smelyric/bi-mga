Guidlines:
Full documentation https://courses.fit.cvut.cz/BI-MGA/tutorials/12/index.html
Rename directory Empty to your username.
Edit only things in your (username) directory.
Don't rename config.json file (you can and should change the content of the file).
You can create as many directories with images as you want inside your main directory.
You can create as many layouts (.json) as you want and list them in config.json.
You can create as many sectors in your layout as you want.
For testing open mga_preview.html.
When testing don't forget to type your username in the Directory input box, then select single layout to test.
Every sector with timing: 0 will be user controlled (simultaneously).
You can check example presentation in directory example.