== Dokumentace k semestrální práci - vektrorová část.

=== První vektorizace - automatická

==== Zdrojová bitmapa
Čerpáno z tohoto link:https://farm8.static.flickr.com/7818/33729325158_f040c88b1a_b.jpg[zdroje]:

image::fokker_automatic.jpg[]

==== Pracovní postup

. Předtím, než začnu pracovat s Inkscapem, tak rychle skočím do GIMPu a magickou hůlkou + maskou odstraním vyfocené pozadí. (viz úkol na retuše)
. Odstraněné pozadí nahradím jednolitou barvou (modrou jako obloha).
. Pro efektivitu oříznu.
. Vyexportuji do png.
+
image::fokker_automatic_solid.png[]
. Nyní si otevřu Inkscape.
. Pomocí *File -> Open...* otevřu zdrojovou bitmapu.
. V objektech si ji vyberu.
. Aplikuji *Path -> Trace Bitmap...* s tímto nastavením: 
+
image::steps/step001.png[]
. Hotovo!

==== Výsledek úprav

link:fokker_automatic.svg[]

''''

=== Druhá vektorizace - překryvem

==== Zdrojová bitmapa
Čerpáno z tohoto link:https://cdn.instructables.com/FNP/RR49/HGH70CYG/FNPRR49HGH70CYG.LARGE.jpg?auto=webp&&frame=1&width=1024&height=1024&fit=bounds[zdroje]:

image::fokker_overlap.jpg[]

==== Pracovní postup

. Zapnu Inkscape.
. V Inkscapu si zapnu okna pro editaci výplně, vrstev, objektů a zarovnání (později i textu).
. Pomocí *File -> Open...* otevřu zdrojovou bitmapu.
. Zdrojovou bitmapu si zduplikuji.
. Vytvořím si 6 vrstev v *Layers* pro:
.. Vrchní zdrojovou bitmapu, která bude částečne průhledná.
.. Text, kterou využiji úplně nakonec na popisky.
.. Letadlo zepředu.
.. Letadlo shora.
.. Letadlo z boku.
.. Spodní zdrojovou bitmapu, která nebude ani trochu průhledná.
+
image::steps/step002.png[]
. Pro každou z těchto vrstev si budu v průběhu modelování různě vypínat a zapínat vykreslování pro lepší viditelnost.
. Zvolím pořadí modelování pohledů: zepředu, shora, z boku.
. Začnu pomocí elips _[F5]_ a čtverců _[F4]_ modelovat objekty nejvíce v popředí.
. První je tedy na řadě vrtule:
.. Střed vymodeluji jednoduchou kružnicí s okrajem. (Již při těchto krocích si na dolní paletě přibliženě vybírám barvy.)
.. Listy vrtule jsou náročnější, protože netvoří klasickou elipsu. Použiji dvě elipsy (jednu pro užší část listu, jednu pro širší část).
.. Pomocí *Path -> Intersection* z těchto dvou elips udělám jeden objekt - množinové operace budu dále používat k modelování všech tvarů, které potřebuji (za celý proces modelování letadel nesáhnu ani jednou na uzly jednotlivých tvarů, všechny součásti letadel jsou vymodelovány množinovými operacemi a dvěma základnímy tvary!) pozn.: modelovat profil křídla bylo velmi bolestivé
+
image::steps/step000.png[]
. Pro snadnější a uniformější barvení používám kapátko _[F7]_
. Pro snadnější organizaci objektů si související části letadla groupuji _[Ctrl] + [G]_.
+
image::steps/step003.png[]
. Pokračuji v modelování všech pohledů na letadlo.
. V pohledu shora a z boku použiji na kryt motoru lineární gradient pro přidání lesku.
. Další lineární gradient použiji v pohledu shora pro spodní křídlo pro dodání plastického dojmu.
+
image::steps/step004.png[]
. Nakonec do textové vrstvy jdu dodělat popisky letadla pomocí textového nástroje _[F8]_.
. Čáry, které spojují popisky s letadlem, jsem nakonec křivkami _[Shift] + [F6]_ udělal.
. Hotovo!

==== Výsledek úprav

link:fokker_overlap.svg[]

''''

=== Třetí vektorizace - obtahem

==== Zdrojová bitmapa
Čerpáno z tohoto link:https://s3.eu-west-2.amazonaws.com/abpic-media-eu-production/pictures/full_size_0393/1591341-large.jpg[zdroje]:

image::fokker_outline.jpg[]

==== Pracovní postup

. Zapnu Inkscape.
. Pomocí *File -> Open...* otevřu zdrojovou bitmapu.
. Objektu zdrojové bitmapy nastavím částečnou průhlednost.
. Pomocí elips _[F5]_ a křivek _[Shift] + [F6]_ začnu modelovat.
+
image::steps/step005.png[]
. Přijdu na to, že je snažší postupovat systematicky zepředu dozadu.
. Tímto způsobem vymodeluji celé letadlo -> hotovo!

==== Výsledek úprav

link:fokker_outline.svg[]

=== Čistý čas strávený úkolem

10 hodin.

=== Poznámky

Byl jsem řádně vyškolen, že opravdu musím spamovat _[Ctrl] + [S]_, protože v průběhu modelování stihnul Inkscape spadnou asi čtyřikrát.