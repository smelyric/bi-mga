# 12. přednáška

## Motion capture

- Apple má 3D scanner v multiobjektivech
- Capture reality - 3D model z fotek, C4D
- Fabrication - 3D print počítačové grafiky
- MakerLabs - Průša v Holešovicíh
- Animátor za den zanimuje 4-7 sekund
- Loutkaři a švadleny
- Laboratoř chůze - analytika chůze
- Monkey -> Gypsy -> Elektromagnetické vzdálenosti -> Optical offline -> Optical online - vysokorychlostní kamery
- Více jak 30 let starý systém
    - na každém čipu je samostatná závěrka = antiblur system
    - greyscale kamera hledající kuličky, preprocessing už v kameře, submilimetrová kamera, 2MGPX 5-10 metrů
    - vždy se to bere více způsoby
    - až 1000 fps
- kalibrace přes jeden objekt pro všechny kamery
- kalibrace člověka = váha, výška
- jeden bod asymetricky
- systém kara - na Plzeňské univerzitě
- kamera před obličejem - body fixou
- T-Pose

# 11. přednáška

- Jaroslav Křívánek - C(K)orona
- HDR mapy osvětlení, hdrheaven.com
- global illumination rendering - každý objekt se může stát světlem
- počítačová grafika dělá reverzní rendering (od oka, do světla)
- **raytracing** - hází paprsky z oka, rekurentně řeší objekty: světla, bod lomu, odrazivost, difuzní
- lighttracing = reverzní raytracing -> šum
- ostrý stín je nechtěný
- pojmy:
	- radiance = kolik světla vyzařuje - kandela = radianec svíčky z 1 metru
	- BRDF = bi-directional radion distribution funtion = charakteristika povrchu -> rendering equation
		- specular = velikost plosky pri odrazu, metalické, zrcadlo ma bod
		- perfectly diffuse - opak zrcadla, hemisfera, všesměrové osvětlení
			- specular a diffuse složky se sčítají
		- emitace - jestli objekt svítí
		- SSS = subsurface scaterring - pevnější krusta, prosvícení dovnitř, nepřímý zdroj světla, (prosvícení uší)
		- isotropický - směrově stejně
		- anisotropický - jednosměrně broušené povrchy, vlasy (fuj), manšestr, samet
- photon map - radiance caching = jak to optimalizovat - první instance odstraní jednu rekurzi tím, že objektům dá novou "texturu" světelnou, znovu - umí caustic
- Cornell Box - srovnávací způsob
- goniometr - první film, který to používal je Matrix
- **jak spočítat difuzní reflektanci cos Y = vektor(v) * vektor(r)** //skalarní součin
	- tlustotu toho odrazu vypočítám exponentem
	- Phong *X* Blinn-Torrance
	- + ambient illumination = obecné osvětlení = totální hack
- fresnel reflection - světlo při krajní poloze (šupinky barev)
- principal shader = standardizace shaderů



# Předchozí nemarkdownové poznámky

10. prednaska:
doba prizpusobeni intenzite svetla

ideal = rozsviceno, ale nevime odkud

ne jenom podle norem!

veliciny:
	svetelny tok
	intenzita osvetleni
	merny svetelny tok
	osvetlenost - luxy (svetelny tok / plocha)

stejne osvetleni na jinou barvu steny vnimame uplne jinak (na bilou a cernou stenu)

RGBw - w (bile ledky) redi sytost barev

teplota chromaticnosti - kdyz zahrejeme zelezo na 2700 K (neco jako svicka), tak jak to vypada

denni svetlo ma plny spektrum - umele nemaji homogenitu
take je potreba mega moc luxu, na kvalitni zobrazeni barev pri vysoke teplote chromaticnosti

na teple barvy, teple svetlo and vice versa

3 druhy LED:
	high power: jednotlive
	mid power: plosne
	low power: orientacne
	
omegadulezite termo management LED (pasivne v naproste vetsine)

u LED klesa svitivost - index L90/B10 = po 50,000 hodinach provozu se svitivost u max 90% snizi o 10%

pro praci se svetlem ho musim moct ridit
	spot
	flanela
	elipsa - muzu 5 obrazu nasvitit jednim svetlem
	wallwasher - homogenni
	zoom

gramatika svetla: potreba postupne, nemusi mit stejnou teplotu (pozadi spis studenejsi)
	ambientni osvetleni - definice houbky prostoru, vnimani svetlosti prostoru, napr. wallwashing, vnimame 4:1 vic vertikalni svetlo oproti horizontalni
	akcentove osvetleni (focal glow) - plasticita, hierarchie, stiny, dramaticnost (kontrast mezi ambientnim a akcentovym alespon 2:1, velmi akcentovany 5:1, max hrana 10:1)
	scenicke svetlo - projekce, barva, brilance
	
wallwashing
spotlighting
floodlighting
orientacni osvetleni

da se rozsvitit strop - ceiling washlight

navrh:
	vizualizace, render - data realnych svitidel
	nastaveni na zaver (zkontrolovat ty kokoty 

vypinac nemusi byt on/off - i naprogramovane osvetleni, next level je bluetooth

expozicni osvetleni:
	podporit umelecke dilo, ochrana exponatu (UV zareni poskozuje nejvic) - luxhodiny nebo max 40 luxu, aby neoslnovalo
	viz gramatika svetla
	eliminace stinu pozorovatele
	intenzita svetla x vzdalenost pozorovatele
	pri nasviceni skla pozor na odrazy - muzeini uhel je 30 stupnu

//galerie ve Vidni prej poggers

rezidencni osvetleni:
	propojovani prostoru

administrativni osvetleni:
	dneska uz nepotrebujeme 500 luxu na stole, kdyz pracujeme na monitorech
	usetri to provozni naklady +- 50 %

lifecycle cost!

8. prednaska:
animation nodes

3 zakladni 
posuv
scaling
rotaci

homogenni souradnice - body v nekonecnu, projektivni transformace
+1 dimenze -> (x, y, z, h), h - 1 (0 pro nekonecno) -> x/h, y/h, z/h
x == M*x + t -> homogenizovane(x) == M*x

kdyz je h == 1, tak se nic nedeje pri tranformacich

1/w

homogenni tranformace - proste jenom tri ruzne matice RTS - !!nekomutativni:
	translace:
		!!!nasobeni matic -> x = x + tx, y + tx, z + tx
	scale:
		nasobeni
	rotace (z axis)?:
		cos ; -sin ; 0 0  *  x   =   vysledek
		sin ;  cos ; 0 0     y
		0   ;    0 ; 1 0     z
		0   ;    0 ; 0 1     1

GPU umi vektory - matice pocitat v jednom kroku

hierarchie sceny - parenting

uplne nakonec kamera = matice ( h=(0;1) - vydeli se to tim )

blender kit

tanformace se ukladaji do uzlu

(QR faktorizace / SVD) dostava zdroj z debilniho formatu

pri perspektivnim pohledu potrebuji planarnich 8 bodu, bundle adjustment - RANSAC

projektivni transformace:

1 0 0   0
0 1 0   0
0 0 1   0
0 0 1/d 0

d = ohniskova vzdalenost
----------------


nejcastejsi je povrchova reprezentace, pomoci trojuhelniku nejlip, ale dnus uz nekonvexni N-GON

indexed face set = ocislovane vrcholy, pravidlo prave ruky - normala

pro zaobleni se prumeruji normaly facu

!!!jak vypocitam normalu trojuhelniku:
	mam vrcholy V1, V2, V3 - vektorovy soucin: v1 x v2 = n1 smer normaly, normalizuji velikost: n1/sqrt(V1^2 + V2^2 + V3^2)

lite.com

maliruv algoritmus, serazeni odazu dopredu ->
z-buffer- vsechno je v nekonecnu

2.5D = 2D + z-buffer

6. prednaska:
- jpeg blocking artifact problem
= GOP (group of pictures) - podle delky - 1, 2, long - distribucni
- MPEG2 pro DVBT a DVBS
- MPEG2-TS (transport stream) - muze v sobe mit i vyssi mpeg, jedna se pouze o obalku
- H262 = MPEG2 -> H264 = MPEG4
= MP4
    - posila, jak defiltrovat
    - slice
    - dynamicke bloky
    - rozliseni na ctvrtinu pixelu
    - GOP pro slicy zvlast
    - pohyb zakodovan cislama
    
- VC1 > MPEG4
    - Microsoft
    - intro koeficient prediction
    - pred zakodovanim prestrukturuje
    
- VC2 > H265 - pro 4k a 8k
    - SMPTE VC1 - public
    
- HBO GO - na zacatku sum checkuje internet



5. prednaska:
= NDI - vyuziva ztratovou kompresi, delay
- tolerance 4 framy, 7 framu max
- formaty pro ukladani obrazu:
	- ztratova/bezztratova komprese
	- standardni format / proprietarni standard
	- bitmapa / vektor / 3D / video / audio

- entropie - teorie informace - snazime se o nejmensi pocet bitu

- GIF
	- alfa kanal, 8 bitove barvy (paleta), animace, umel se nacitat postupne (Pokac)

- PPM
	- textove ulozeni obrazku

- PNG
	- neztratova komprese

- TIFF
	- puvodne komercni, pro tisk, ma metadata, az 16 bit/kanal, velke obrazky (4294967295^2), muze byt komprimovany i nekomprimovany

- JPEG (JFIF)
	- nakope barvy na polovinu 4:2:0, ztratove, nastupce JPEG2000
	- jak funguje komprese JPEG:
		- zamereno na sum - furiova transformace = suma sin a cos o ruznych x -> sinova/cosinova transformace -> digital ->
		-> DCT pro kladna cisla (8x8 pixelu - jine frekvence pro jine tvary, sum v pravo dole)
		- kvantizace - ztrata sumu - #test
		- zbytek entropicky enkoder
		- pozor 8x8 neni nezavisle

- JPEG2000
	- near lossless, < test> wavelet komprese - bazove funkce, celociselna aritmetika - HAARova baze, DAUBECHIES
	- filmy v kinech
	- umi i neztratovou kompresi - SPIHT
	- i ve hrach, gpu akcelerace
	- dekompozice umi preview v ruznych stylech, i region of interest (VR) - mega cool, maly Gbps

- OpenEXR
	- HDR data

---

Video:

	MPEG1 - micro
	MPEG2 - DVD, MPEG2-Layer3 (MP3)
	MPEG3 neexistuje
	MPEG4 - pro mobilni zarizeni -> H264/AVC - max 4k25 -> H265
	MPEG7 - metadata
	MPEG21 - vyhledavani metadat
	VC-1 - od Microsoftu
	VC-1

4. prednaska:
- 4k projektory rotuji
- digitalni prenos ma vertikalni (nastavuje se, desitky Hz) a horizontalni frekvenci (kHz)
- pixel clock - casovani jednoho pixelu (MHz)
- 1.5 Gb/s
- TMDS
- single link (malo kabelu), dual link (vic kabelu, mensi vzdalenost)
- kabely - UTP, FTP, SFTP
= kolik ma display port sirku pasma = 21.6 Gbps
= DVI, displayport
- SDI kabely v televizich
= HD-SDI - 1,5 Gbps - do 50 metru je uplne v pohode
- 3G-SDI - 3 Gbps
- 12G-SDI - ...
- HDR - high dynamic range - dokaze zobrazit vic svetlosti najednou
= HDCP - zpusob ochrany autorskych prav v kabelech (sifrovani)
- EDID - monitor nazpatek posle info o sobe, hlavne treba rozliseni, EDID manager managuje rozliseni, funguje i hardwarove
- CEC - ovladani pres HDMI
- HDMI 2.0 - 15 Gbps, HDMI 2.1 - 48 Gbps - stejne kratke kabely
- HDBaseT - gigabitovy kabel, 8 Gbps, az 100 metru
- USB-C - oboustranne napajeni, Thunderbolt 3
- NDI - ethernet, mega dlouhe kabely, ultra komprimace

3. prednaska - barvy:
- 400 - 700 nm, svetlo
- tri typy vnimani svetla - Grassmanovy zakony; dominantni vlnova delka = hue, cistota barvy = saturation, intenzita = brightness
- aditivni skladani barev
- cernobile ma oko vetsi rozliseni, periferne je cernobile
- pri pohybu se snizi rozliseni, zvysi frekvence
- cervena nejzajimavejsi, modra nejmene
- vetsi rozlisovaci schopnost ve svislem smeru
- rady:
	- 4-6 ruznych barev
	- nekreslit male objekty a cary modre
	- na pozadi ne cervenou a zelenou
	- nekreslit vedle sebe syte barvy ve spektru
= XYZ (CIE diagram)
- 4:4:4 -> 4:2:0
= co je to gamut? - spektrum, v jakem muze zarizeni zobrazit barvu (monitor, tv, tiskarna)
= smluvni bila
- gama korekce je znelinearneni signalu, prokladani
- NTSC x PAL
- linearni, linearizovana gama
= ktery barevny prostor se pouziva (tv - YUV~YCbCr, pocitac- RGB, tisk - CMYK)

predchozi:
- nejlepsi blur = mitchel
- extrapolace, interpolacni vzorec
- A0 ma plochu 1m^2, pomer stran 1:sqrt(2)
- format PAL (768x576) -> 720x576
- aliasing, anti-aliasing
- shannonuv teorem
- 1/15 sekundy je realtime
