# Dokumentace
## k semestrální práci v předmětu BI-MGA v roce 2019 - retuš

### První retuš

#### Zdrojové obrázky

- Dejvický kampus, vlastní fotografie:

   ![Kampus](composition1_source_background.jpg)
   
- Letadlo (Fokker Dr.I), stažená fotografie z [tohoto zdroje](https://s3.eu-west-2.amazonaws.com/abpic-media-eu-production/pictures/full_size_0393/1591341-large.jpg):

   ![Fokker](composition1_source_fokker.png)

- Fotka mé osoby, vlastní fotografie:
   ![Já](composition1_source_me.jpg)

#### Pracovní postup

- Otevřu si GIMP, ve kterém si otevřu stažený obrázek letadla, naduplikuji vrstvu letadla, abych měl případně zpětnou referenci, nastavím ji jako invisible (odzaškrtnu oko ve vrstvách).
- Po pokusech s magickou hůlkou dojdu k názoru, že bych stejně všechny hrany musel začišťovat, takže zvolím rovnou vytvoření prázdné masky (Add Layer Mask).
- Štětcem, na kterém střídám černou a bílou, různé tloušťky a *Hardness* (*Spacing* nastaven na 1 pro plynulost) obtáhnu letadlo tak, aby pozadí bylo průhledné (na pozadí je maska černá). Pomáhám si přidáním celobílého, celočerného a jednobarevného pozadí pro zvíraznění ořezu.

   ![_](composition1_steps/step01.png)

- Letadlo na zdrojovém obrázku stojí v trávě a malý kus kol kvůli tomu chybí. Pomocí *Clone Tool* (jako zdroj vezmu kus kola, který nechybí) domaluji (pro jistotu do další vrstvy) kola do správného tvaru.

   ![_](composition1_steps/step02.png)

- Vzhledem k tomu, že letadlo na zdrojovém obrázku pro druhou kompozici má bílou směrovku, tak si znovu nakopíruji vrstvu letadla (backup) a štětcem přebarvím směrovku na bílo, stín přidám pomocí *Clone Tool*.

   ![_](composition1_steps/step03.png)

- Do GIMP vrstev přidám fotografii pozadí kampusu (dám ho dospodu).
- Upravím *Image > Canvas Size...* aby bylo vidět tak akorát pozadí pro hezkou a přirozenou kompozici s letadlem
- Pomocí *Move Tool* si nastavím pozice jednotlivých komponent. Pokud chci pohybovat s více vrstvami najednou, tak si je v okně s vrstvami linknu.

   ![_](composition1_steps/step04.png)

- Do GIMP vrstev přídám fotografii mé osoby, ořez pomocí masky provedu stejně jako s letadlem. Na kraji vlasů se snažím přidat více průhlednosti pro přirozenější kompozici. Částečně zprůhledním sluneční brýle pro uspokojení OCD.

   ![_](composition1_steps/step05.png)

- Nakopíruji vrstvu s mou osobou (backup).
- Pomocí *Scale Tool* a *Move Tool* se usadím do letadla.

   ![_](composition1_steps/step06.png)

- Nakopíruji vrstvu s letadlem,
- Magickou hůlkou označím letadlo v masce (tím velmi přesně vyberu viditelný obrys letadla).
- Velkým (opravdu velkým = 500px+) přemaluji vybranou oblast na černo (mám černou siluetu letadla).
- Aplikuji masku.
- Pomocí *Perspective Tool* naaranžuji černou siluetu letadla pod letadlo jako stín tak, aby to v kompozici dávalo smysl (kampus a mou osobu jsem fotil tak, aby stíny souhlasili).
   ![_](composition1_steps/step07.png)

- Kvalita fotografie kampusu je nízká, takže aby letadlo zapadlo, aplikuji na letadlo mirný filtr šumu. 
   
   ![_](composition1_steps/step08.png)

- Přidám popisek pomocí *Text Tool*. 

   ![_](composition1_steps/step09.png)

#### Výsledek úprav

![Výsledek](composition1_result.png)

Po použití [machine learningu](https://bigjpg.com/) pro upscale na téměř 8K:

![Upscaled výsledek](composition1_result_upscaled.png)

---

### Druhá retuš

#### Zdrojové obrázky

- Budova FIT se sloupem, vlastni fotografie:

   ![Budova](composition2_source_background.jpg)

- Letadlo, stažená fotografie z [tohoto zdroje](https://farm4.staticflickr.com/3251/5711732243_7d9bd5a9bf_b.jpg):

   ![Fokker](composition2_source_fokker.jpg)

- Fotká mé osoby - horní část (foceno ve studio s omezenou výškou):

   ![Já - horní část](composition2_source_me_top.jpg)

- Fotka mé osoby - spodní část:

   ![Já - spodní část](composition2_source_me_bottom.jpg)

- Olej, stažená fotografie z [tohoto zdroje](https://t1.uc.ltmcdn.com/en/images/1/3/9/how_to_remove_oil_stains_from_shoes_931_600.jpg):

   ![Olej](composition2_source_oil.jpg)

#### Pracovní postup

- Otevřu si GIMP, ve kterém si otevřu stažený obrázek letadla, naduplikuji vrstvu letadla (backup).
- Přidám prázdnou masku.
- Pomocí masky oříznu letadlo stejně jako v první retuši.

   ![_](composition2_steps/step01.png)

- Do vrstev přidám budovu FIT.
- Upravím *Image > Canvas Size...* pro dobrou kompozici.
- Poposouvám si letadlo tak, aby dobře zapadalo, v průběhu si uvědomím, že lépe zapadá, když ho flipnu okolo svislé osy (letí na druhou stranu). ![_](composition2_steps/step02.png)
- Na letadlo fontem, který připomíná dobu slavného Red Barona - Manfred von Richthofena, napíšu české letecké označení OK-RED. Zarovnání upravím po převedení na bitmapu *Perspective Tool*em.

   ![_](composition2_steps/step03.png)

- Přidám novou prázdnou vrstvu.
- Do nové vrstvy nakreslím štětcem s vhodnou texturou za letadlo kouř téměř bílou barvou se zapnutou dynamikou (nasimuluji slábnutí kouře).
- Na kouř aplikuji *Gaussian Blur* pro zjemnění kouře.

   ![_](composition2_steps/step04.png)

- Naduplikuji pozadí.
- V naduplikovaném pozadí si lasem vyberu sloup (je krásně rovný, jde to jako po másle).
- Vytvořím masku na pozadí *From Selection*

   ![_](composition2_steps/step05.png)

- Přidám novou prázdnou vtstvu.
- Do nové vrstvy nakreslím štětcem se stejnou texturou kouře a barvy, ale menší velikostí pokračování kouřové stopy.
- Hodím vrstvu "za" vrstvu se sloupem.
- Na kouř v pozadí aplikuji silnější *Gaussian Blur*. Lehce kouř zprůhledním pomocí *Opacity* vrstvy.

   ![_](composition2_steps/step06.png)

- Do vrstev přidám zdrojovou fotku oleje.
- Při přidávání masky použiji *Grayscale copy of layer*. Funguje překvapivě dobře.
- Pomocí *Perspective Tool* dám odstříknutý olej z letadla na sloup.

   ![_](composition2_steps/step07.png)

- Stín letadla vyrobím jako v první retuši. Z kompozice vychází pouze na kousek ve spodní části sloupu.

   ![_](composition2_steps/step08.png)

- Do vrstev přidám vrchní a spodní část mé osoby.
- Masku upravím tak, aby se tělo dalo pěkně spojit na rozmezí bunda - spodek těla. Zbytek ořezu provedu stejně jako u letadla.

   ![_](composition2_steps/step09.png)

- Naduplikuji letadlo.
- Maskou vyříznu pouze část spodního křídla (za kterou se bude moje osoba držet).

   ![_](composition2_steps/step10.png)

- Zarotuji a nascaluji moji osobu tak, aby se kompozičně "držela" vrchu spodního křídla letadla.

   ![_](composition2_steps/step11.png)

- Přidám popisek **Fokker Dr.I**.

   ![_](composition2_steps/step12.png)

- Vrstvy seřadím tak, aby kompozice dávala smysl (od objektu nejvíce v popředí):
  - nápis OK-RED
  - vyřízlé spodní křídlo
  - má osoba - vršek
  - má osoba - spodek
  - letadlo
  - kouř v popředí
  - olej
  - stín
  - sloup
  - kouř v pozadí
  - pozadí budovy FITu.
- Na vrstvy které se "pohybují" (nápis OK-RED, vyřízlé spodní křídlo, má osoba - vršek, má osoba - spodek, letadlo, stín) aplikuji *Linear Motion Blur* ve směru pohybu.

#### Výsledek úprav

![Výsledek](composition2_result.png)

Po použití [machine learningu](https://bigjpg.com/) pro upscale na 12000x9000:

![Upscaled výsledek](composition2_result_upscaled.jpg)

---

### Čistý čas strávený úkolem

Velmi hrubý odhad je 20 hodin.
